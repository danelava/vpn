apt update && apt upgrade
apt install -y vim htop openvpn iptables-persistent
sed -i '/net.ipv4.ip_forward/s/^#//' /etc/sysctl.conf
sysctl -p
cat /proc/sys/net/ipv4/ip_forward
mkdir -p /opt/vpn/log
cd /opt/vpn/
cp -af /usr/share/easy-rsa/ ./
cd easy-rsa/
mv vars.example vars
vim vars
./easyrsa init-pki
./easyrsa build-ca nopass
./easyrsa build-server-full server nopass
./easyrsa gen-dh
openvpn --genkey --secret pki/tls.key
mkdir /etc/openvpn/keys
cp -R pki/ca.crt /etc/openvpn/keys/
cp -R pki/dh.pem /etc/openvpn/keys/
cp -R pki/tls.key /etc/openvpn/keys/
cp -R pki/private/server.key /etc/openvpn/keys/
cp -R pki/issued/server.crt /etc/openvpn/keys/
vim /etc/openvpn/server.conf

./easyrsa build-client-full client01 nopass

