#!/bin/bash

if [ -z "$1" ]
  then
    echo "Please input key name. Example: ./gen.sh cl01"
fi

KEY_DIR=pki/private
CERT_DIR=pki/issued
PKI_DIR=pki
OUTPUT_DIR=/var/www/html/v
BASE_CONFIG=base.conf

./easyrsa build-client-full $1 nopass

cat ${BASE_CONFIG} \
    <(echo -e '<ca>') \
    ${PKI_DIR}/ca.crt \
    <(echo -e '</ca>\n<cert>') \
    ${CERT_DIR}/${1}.crt \
    <(echo -e '</cert>\n<key>') \
    ${KEY_DIR}/${1}.key \
    <(echo -e '</key>\n<tls-auth>') \
    ${PKI_DIR}/tls.key \
    <(echo -e '</tls-auth>') \
    <(echo -e 'key-direction 1') \
    > ${OUTPUT_DIR}/${1}.conf

cat ${OUTPUT_DIR}/${1}.conf