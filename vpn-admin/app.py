from flask import Flask, render_template
import subprocess, string, random

app = Flask(__name__)

@app.route("/")
def main():
    return render_template('base.html')

@app.route("/generate")
def gen_key():
    key_name = ''.join(random.choices(string.ascii_lowercase + string.digits, k=4))
    try:
        output = subprocess.check_output(
            ['sh', './gen.sh', key_name],
            stderr=subprocess.STDOUT,
            timeout=3)
    except subprocess.CalledProcessError as err:
        print("Error: ", err.returncode, err.output)
        return render_template('base.html', error=err.output)
    return render_template('base.html', key_name=key_name)