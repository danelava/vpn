#!/bin/bash

if [ -z "$key_name" ]
  then
    echo "Please input key name. Example: key_name=fv1"
    exit 1
fi

apt update && apt upgrade -y
echo iptables-persistent iptables-persistent/autosave_v4 boolean true | debconf-set-selections
echo iptables-persistent iptables-persistent/autosave_v6 boolean false | debconf-set-selections
apt install -y vim curl openvpn iptables-persistent
sed -i '/net.ipv4.ip_forward/s/^#//' /etc/sysctl.conf
sysctl -p
cat /proc/sys/net/ipv4/ip_forward

iptables -t nat -F
iptables -t nat -A POSTROUTING -s 192.168.8.0/24 -o tun0 -j MASQUERADE
iptables-save > /etc/iptables/rules.v4

cd /etc/openvpn
rm -f client.conf
curl http://$vpn_ip/v/$key_name.conf -o client.conf
systemctl enable openvpn
systemctl restart openvpn
sleep 5
curl ifconfig.co
